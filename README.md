# amoter

## Who Am I?

Howdy, I'm Anna Liisa. That's pronounced 'Ahna-Leesa' and I prefer to be addressed by my entire name.

My pronouns are she/her. 

I'm located in the Munich Germany area, so I operate in Central European Time.

### Background 

I was born and raised in Europe to two Dutch parents, and moved to the US in middle school.
I speak Dutch, German and Spanish, all fluently at one time, tho they're pretty rusty from lack of use right now. I recently relocated from the US to Germany, so I'm working on getting my German back up to snuff, and expanding my vocabulary.

My first career was in counseling and coaching, applying my MA in Psychology to help people untangle intra- and inter-personal problems. A few years ago I decided to pivot into Software Engineering, and fell in love with solving technical problems. I especially appreciate the breadth and depth that Infrastructure Engineering requires, with a systems-level perspective yet still very detail oriented.

Now I'm excited to combine my technical and people skills to support the Infrastructure Department as an Engineering Manager.

### Values

These map to many of GitLab's values and subvalues, but using my own words:

- Whole Heart: I lead with my whole heart. To me this means putting connection and empathy first, assuming positive intent, taking time to build trust, and having the courage to have hard conversations. Building psychological safety also falls under this value, because I want to work at a place where everyone feels safe to bring all of ourselves to work.
- Service: I am here to empower and unblock others so we can all deliver our best together.
- Dive in: I have a tendency to dive with two feet into the deep end of the pool.
- See it, Fix it: I believe in addressing problems as I see them whenever possible. 
- Speak up, Speak out: if I see a problem that I can't fix, I will speak up about it. This includes things like organization level dysfunction and/or lack of diversity and inclusion.
- Better Together: I prefer to work in collaboration with others. I believe strongly in the benefits of pair programming.
- Process in service of Results: My mind tends to look for processes and structures to improve the way we work. But that is always in service of results. Process for the sake of process is a waste of time.
- Life Long Learning: I have so much to learn, and a never-ending curiosity about the world.
- Life is short, so enjoy it: I am a hedonist at heart, and humor is not optional. (Laughing at my jokes, however, is absolutely optional!)

### Expectations

#### from my manager

- Direct and actionable feedback in our 1:1s so I can improve.
- Guidance on organizational dynamics (For example, I need to know when I'm pushing too hard or not enough, being ineffective inside or outside my team, or caring about the wrong things in context of what the organizations priorities are).
- Context for how my team's work fits into the bigger picture.
- Clarifying expectations.
- Trust and empowerment to do the job I'm here for.

#### from my peers

- Direct and actionable feedback about how my actions impact them so I can improve.
   - this includes technical feedback - I want to hear when there's a better way to do something, and why.
- Partnership and collaboration.

#### from my direct reports

- Direct and actionable feedback about how my actions impact you so I can improve.
- Communicate when you are struggling, unhappy or disengaged, or see a pattern that is slowing us down - I want to hear it all.
- I am here in service to you. So please let me know how I can help!

#### from my employer/upper management

- Direct communication about where we are and where we want to go.
- Acknowledgement of areas of growth, both personally and for the department/company.
- Leading by example.
- Sincerely inviting feedback.

### Schedule

- I work in CET time zone
- I am not a morning person but will get up early if it's important to the team
- I tend to be at my best mental acuity between 10am and 2pm PST

### Communication

- I tend to err on the side of over-communication vs under. 
- I strive to acknowledge my shortcomings and own my mistakes explicitly.
- You may have picked up on a theme around feedback. I *always* welcome feedback. I prefer that it be actionable and specific, and delivered as soon as possible, but I appreciate any effort at all to provide feedback. I consider it a gift and an act of generosity, and strive to provide it as much as I receive it.
    - For critical/constructive feedback, I prefer private communication vs a group setting. 
    - From my direct reports I prefer feedback in a group setting over withholding feedback altogether.

### Style

- I believe that everyone, at *all* times, is doing the best they can given the resources they have. I aim to remember that at all times, tho I don't always succeed. Reminders are always welcome and appreciated.
- I appreciate direct, forthright communication and try to offer the same.
- I also like to have a sense of humor. I try to be sensitive to the fact that there are times when humor is not called for.
- I believe meetings with one person responsible for facilitating said meeting, taking on time management and getting through an agenda makes for much more effective and productive meetings.
- I would describe my natural management style a collaborative, coaching style. While there may be occasions where I need to relay direct "orders", I prefer to explore together, and ask good questions that will remind who I'm speaking with of the solutions they probably already had in them all along.
- I believe empathy is not optional when it comes to people leadership.


### Challenges

In an effort to normalize chronic illness and neurodiversity at work, I'm including these sections here.

#### physical

I have had a lengthy, ongoing struggle with chronic illness. For me that primarily involves:

- migrating body pain
- frequent migraines/headaches
- frequent nausea
- brain fog and word recall issues
- bouts of insomnia
- chronic fatigue

These symptoms are often cyclic in nature, with periods of feeling better and periods of feeling worse. I've noticed a pattern of worsening in Spring and Autumn. I am doing what I can to address this, and while well-meaning, suggestions of treatments/remedies are not generally helpful. I do appreciate understanding for not being at my best at times, and needing to take some time for self-care and doctors' appointments now and then.

#### mental

- I'm an introvert with a form of social anxiety that has me replay social interactions *after the fact* and pick apart my behavior (often in the middle of the night). It's one of the reason why feedback is so important to me, because it gives me a reality check and often will help to decrease my anxiety.
- Crowds/large groups are especially difficult. Spending all day in a physical office surrounded by people is deeply exhausting, which is why remote work is such a great fit for me.
- I have ADD. I am taking medication to address this, but when combined with days of brain fog due to my autoimmune disorder, focus can be a challenge.
- True to ADD, I can also become hyperfocused at times, which can cause me to lose track of priorities.
- Accountability structures can be a helpful tool to get me back on track.
- As vocal as I am, it may surprise people to know that conflict is very challenging for me, and takes a toll mentally and physically.

### Hobbies

- Horseback riding
- Getting out in nature with my family and dogs
- Snowboarding badly, and skiing even worse
- Singing, preferably with others
- Dancing, preferably when no one is watching
- Getting a little obsessed with learning about something. Past obsessions include:
    - wine and winemaking
    - bee keeping and apitherapy
    - lactation

