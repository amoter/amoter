require 'gitlab'
require 'pp'
require 'awesome_print'

Gitlab.configure do |config|
  config.endpoint       = 'https://gitlab.com/api/v4' # API endpoint URL, default: ENV['GITLAB_API_ENDPOINT']
  config.private_token  = 'your_token_here'       # user's private token or OAuth2 access token, default: ENV['GITLAB_API_PRIVATE_TOKEN']
  # Optional
  # config.user_agent   = 'Custom User Agent'          # user agent, default: 'Gitlab Ruby Gem [version]'
  # config.sudo         = 'user'                       # username for sudo mode, default: nil
end

usernames = ['rnienaber']
date = '2020-01-01'
projects = Hash.new{|h,k| h[k] = Gitlab.project(k)}
per_page = 100

def get_events(username, date, current_page, per_page)
  user = Gitlab.user_search(username)[0]
  user_id = user.id
  events = Gitlab.user_events(user_id, { after:date, per_page:per_page, page:current_page})
  ap "Getting events for #{username} on page #{current_page} and found #{events.size}"

  events
end

count = 0


usernames.each do |username|

  puts "#{username} since #{date}"
  puts "https://gitlab.com/users/#{username}/activity"
  activity = []

  should_continue = true

  (1..100).each do |current_page|

    if !should_continue
      break
    end

    events = get_events(username, date, current_page, per_page)

    if events.size < per_page
      should_continue = false
    end

    events.each do |event|

      if !event.project_id
        break
      else
        project = projects[event.project_id]
      end


      if event.action_name == "commented on"
        # if event.action_name == "commented on" && event.note.noteable_type == "Issue"
        # activity << "#{event.created_at} Commented on issue in #{project.name} : #{project.web_url}/issues/#{event.note.noteable_iid} - #{event.target_title}"
      # elsif event.action_name == "opened" && event.target_type == "Issue"
      #   activity << "#{event.created_at} Opened Issue in #{project.name} : #{project.web_url}/issues/#{event.target_iid} - #{event.target_title}"
      # elsif event.action_name == "commented on" && event.note.noteable_type == "Commit"
      #   activity << "#{event.created_at} Commented on commit in #{project.name} : #{event.target_title}"
      # elsif event.action_name == "commented on" && event.note.noteable_type == "MergeRequest"
      #   activity << "#{event.created_at} Commented on MR in #{project.name} : #{project.web_url}/merge_requests/#{event.note.noteable_iid} - #{event.target_title}"
      # elsif event.action_name == "accepted" && event.target_type == "MergeRequest"
      #   activity << "#{event.created_at} Accepted MR in #{project.name} : #{project.web_url}/merge_requests/#{event.target_iid}"
      # elsif event.action_name == "closed" && event.target_type == "MergeRequest"
      #   activity << "#{event.created_at} Closed MR in #{project.name} : #{project.web_url}/merge_requests/#{event.target_iid}"
      # elsif event.action_name == "deleted" && event.push_data.ref_type == "branch"
      #   activity << "#{event.created_at} Deleted branch in #{project.name} called #{event.push_data.ref}"
      # elsif event.action_name == "pushed new" && event.push_data.ref_type == "branch"
      #   activity << "#{event.created_at} Pushed to branch in #{project.name} called #{event.push_data.ref}"
      # elsif event.action_name == "pushed to" && event.push_data.ref_type == "branch"
      #   activity << "#{event.created_at} Pushed to branch in #{project.name} called #{event.push_data.ref}"
      # elsif event.target_type == "MergeRequest" && event.action_name == "opened"
      #   activity << "#{event.created_at} MR Opened: #{project.name} : #{project.web_url}/merge_requests/#{event.target_iid} - #{event.target_title}"
      # elsif event.target_type == "Issue" && event.action_name == "closed"
      #   activity << "#{event.created_at} Issue Closed: #{project.name} : #{project.web_url}/issues/#{event.target_iid} - #{event.target_title}"
      # elsif event.action_name == "joined"
      #   activity << "#{event.created_at} Joined Project: #{project.name}"
      # elsif event.action_name == "imported"
      #   activity << "#{event.created_at} Imported something ..."
      # elsif event.action_name == "pushed new"
      #   activity << "#{event.created_at} Pushed something ..."
      else
        # ap event
        # exit
      end

    end

  end

  activity.uniq.sort.each do |activity|
    if activity
      puts activity
    end

  end

  puts "-----------"
  puts ""
  puts count


end
